export class Restaurant {
    id: string;
    name: string;
    latitude: number;
    longitude: number;
    address: string;
    distance?: number;
    categoryName: string;
    categoryIcon: string;
    isOpenMapWindow?: boolean;
}
export class RestaurantDetails extends Restaurant {
    rating: number;
    ratingColor: string;
    ratingBy: number;
    price: string;
    imageUrl: string;
    likes: number;
    reviews: number;
}
