import { Component, OnInit, Output, EventEmitter,} from '@angular/core';
import { Router } from '@angular/router';
import { RestaurantService } from '../services/restaurant.service';
@Component({
  selector: 'restaurant-search',
  templateUrl: './restaurant-search.component.html',
  styleUrls: ['./restaurant-search.component.scss']
})
export class RestaurantSearchComponent implements OnInit {
  @Output() searchQuery: EventEmitter<any> = new EventEmitter();
  constructor(private router: Router, private restaurantService: RestaurantService) { }

  ngOnInit() {
    
  }
  onSubmit(value) {
    this.searchQuery.emit(value);
  }

}
