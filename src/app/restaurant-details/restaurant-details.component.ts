import { Component, OnInit, OnDestroy } from '@angular/core';
import { RestaurantService } from '../services/restaurant.service';
import { RestaurantDetails } from '../models/restaurant';
import { Subscription } from 'rxjs';

@Component({
  selector: 'restaurant-details',
  templateUrl: './restaurant-details.component.html',
  styleUrls: ['./restaurant-details.component.scss']
})
export class RestaurantDetailsComponent implements OnInit, OnDestroy {
  mapZoomLevel = 15;
  restaurantIdList: any[] = [];
  selectedRestaurant: RestaurantDetails;
  subscriptions: Subscription[] = [];
  constructor( private restaurantService: RestaurantService ) { }
  ngOnInit() {
    if (this.restaurantService.nearbyRestaurantIdList.length === 0) {
        this.getNearbyRestaurants();
    } else {
        this.restaurantIdList = this.restaurantService.nearbyRestaurantIdList;
        this.selectRandomRestaurant();
    }
  }
  ngOnDestroy() {
    this.subscriptions.forEach( (subs: Subscription) => {
      subs.unsubscribe();
    });
  }
  private getNearbyRestaurants() {
    const getNearbyRestaurantsSub = this.restaurantService.getNearbyRestaurants().subscribe( (data: any) => {
      if(data.meta.code === 200){
        if (data.response.groups[0].items) {
          this.restaurantIdList = data.response.groups[0].items.map( item => item.venue.id );
          this.restaurantService.nearbyRestaurantIdList = this.restaurantIdList;
          if (this.restaurantIdList.length > 0 ) {
              this.selectRandomRestaurant();
          }
        }
      }
    });
    this.subscriptions.push(getNearbyRestaurantsSub);
  }
  private selectRandomRestaurant() {
      const random = this.generateRandomNumber(0, this.restaurantIdList.length);
      if (random) {
          const selectedRestaurantSub = this.restaurantService.getSelectedRestaurantDetails(this.restaurantIdList[random]).subscribe( 
            data => {
              this.selectedRestaurant = data;
          });
       this.subscriptions.push(selectedRestaurantSub);
      }
  }
  private generateRandomNumber(min , max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
 }

}
