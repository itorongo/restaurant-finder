import { Component, OnInit, OnDestroy } from '@angular/core';
import { RestaurantService } from '../services/restaurant.service';
import { Restaurant } from '../models/restaurant';
import { Subscription } from 'rxjs';

@Component({
  selector: 'restaurant-list',
  templateUrl: './restaurant-list.component.html',
  styleUrls: ['./restaurant-list.component.scss']
})
export class RestaurantListComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  restaurantList: Restaurant[] = [];
  currentLatitude: number;
  currenLongitude: number;

  constructor( private restaurantService: RestaurantService) { }

  ngOnInit() {
    this.currentLatitude = this.restaurantService.currentLatitude;
    this.currenLongitude = this.restaurantService.currentLongitude;
  }
  ngOnDestroy() {
    this.subscriptions.forEach( (sub: Subscription) => {
      sub.unsubscribe();
    });
  }
  onGettingSearchQuery(query){
    if(query){
      this.searchRestaurant(query);
    }
  }
  searchRestaurant(query) {
    const restaurantListsub = this.restaurantService.searchRestaurants(query).subscribe( (data: any) => {
      this.restaurantList = data;
    });
    this.subscriptions.push(restaurantListsub);
  }
  onMouseEnter(restaurant) {
    restaurant.isOpenMapWindow = true;
  }
  onMouseLeave(restaurant) {
    restaurant.isOpenMapWindow = false;
  }

}
