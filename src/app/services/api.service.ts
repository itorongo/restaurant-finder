

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of, Subject } from 'rxjs';
import { catchError,  share } from 'rxjs/operators';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private http: HttpClient) {}

  get(apiurl: string): Observable<Response> {
    return this.http.get<any>(apiurl)
      .pipe(
        catchError(this.handleError())
      );
  }

  post(apiurl: string, body: Object): Observable<Response> {
    return this.http.post<any>(apiurl, body, httpOptions)
      .pipe(
        catchError(this.handleError())
      );
  }

  put(apiurl: string, body: Object) {
    return this.http.put<any>(apiurl, body, httpOptions)
      .pipe(
        catchError(this.handleError())
      );
  }

  private handleError<T>(result?: T) {
    return (error: any): Observable<T> => {
      console.log('Error from handleErr', error);
      if (error.status === 404) {
        console.log('Resource Not found');
      } else if (error.status === 400) {
        console.log('Bad Request');
      } else {
        console.log('Unexpected server error');
      }
      return of(result as T);
    };
  }
}
