import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';

import { Restaurant, RestaurantDetails } from '../models/restaurant';
import { ApiService } from './api.service';

import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

const API_URL = environment.apiServiceEndPoint;
const CLIENT_ID = environment.clientId;
const CLIENT_SECRET = environment.clientSecret;
const VERSION_DATE = environment.versionDate;
const LAT_LON = environment.currentLatitude + ',' + environment.currentLongitude;
const RADIUS = 1000;

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  currentLatitude: number = environment.currentLatitude;
  currentLongitude: number = environment.currentLongitude;
  nearbyRestaurantIdList: any [] = [];

  constructor( private apiService: ApiService) { }

  getNearbyRestaurants() {
      const apiUrl = `${API_URL}explore?client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}
                     &v=${VERSION_DATE}&ll=${LAT_LON}&radius=${RADIUS}&section=food&limit=50`;
      return this.apiService.get(apiUrl);
   }

   getSelectedRestaurantDetails(venueId) {
      const apiUrl = `${API_URL}${venueId}?client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET} &v=${VERSION_DATE}`;
      return this.apiService.get(apiUrl).pipe( map((data: any) => {
         if(data.meta.code === 200){
           return this.prepareRestaurantDetails(data);
         }
      }));
   }

   searchRestaurants(query) {
      const apiUrl = `${API_URL}search?client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}
         &v=${VERSION_DATE}&ll=${LAT_LON}&radius=${RADIUS}&limit=50&query=${query}`;
      return this.apiService.get(apiUrl).pipe( map( (data: any) => {
         if(data.meta.code === 200){
             return this.prepareRestaurantList(data);
         }
      }));
   }

   prepareRestaurantDetails(data) {
      const restaurantDetails: RestaurantDetails = new RestaurantDetails();
      restaurantDetails.id = data.response.venue.id;
      restaurantDetails.name = data.response.venue.name;
      restaurantDetails.latitude = data.response.venue.location.lat;
      restaurantDetails.longitude = data.response.venue.location.lng;
      restaurantDetails.address = data.response.venue.location.formattedAddress.join('<br>');
      restaurantDetails.categoryName = data.response.venue.categories.length > 0 ? data.response.venue.categories[0].name : 'Not given';
      restaurantDetails.categoryIcon = data.response.venue.categories.length > 0 ?
                                       data.response.venue.categories[0].icon.prefix + '88' + data.response.venue.categories[0].icon.suffix
                                       : '';
      restaurantDetails.rating = data.response.venue.rating;
      restaurantDetails.ratingBy = data.response.venue.ratingSignals;
      restaurantDetails.ratingColor = data.response.venue.ratingColor;
      restaurantDetails.price =  data.response.venue.price.message ? data.response.venue.price.message : 'Not given';
      const imgWidth = data.response.venue.bestPhoto.width;
      const imgheight = data.response.venue.bestPhoto.height;
      restaurantDetails.imageUrl = data.response.venue.bestPhoto.prefix + imgWidth + 'x' + imgheight + data.response.venue.bestPhoto.suffix;
      restaurantDetails.likes = data.response.venue.likes.count;
      restaurantDetails.reviews = data.response.venue.stats.tipCount;
      return restaurantDetails;
   }
   prepareRestaurantList(data) {
      let restaurantList: Restaurant[] = [];
      restaurantList =  data.response.venues.map( (item, index) => {
         const restaurant: Restaurant = new Restaurant();
         restaurant.id = item.id;
         restaurant.name = item.name;
         restaurant.latitude = item.location.lat;
         restaurant.longitude = item.location.lng;
         restaurant.categoryName = item.categories.length > 0 ? item.categories[0].name : 'Not defined';
         restaurant.categoryIcon = item.categories.length > 0 ? item.categories[0].icon.prefix + '88' + item.categories[0].icon.suffix : '';
         restaurant.distance = item.location.distance;
         restaurant.address =  item.location.formattedAddress.join();
         restaurant.isOpenMapWindow = false;
         return restaurant;
      });
      return restaurantList;
   }
}
