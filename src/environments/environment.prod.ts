export const environment = {
  gooleMapAPIKey: 'YOUR_GOOGLE_MAP_API',
  apiServiceEndPoint: 'https://api.foursquare.com/v2/venues/',
  clientId: 'FOURSQUARE_CLIENT_ID',
  clientSecret: 'FOURSQUARE_CLIENT_SECRET',
  versionDate: '20180323',
  currentLatitude: 23.793744,
  currentLongitude: 90.404816
};
