// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  gooleMapAPIKey: 'YOUR_GOOGLE_MAP_API',
  apiServiceEndPoint: 'https://api.foursquare.com/v2/venues/',
  clientId: 'FOURSQUARE_CLIENT_ID',
  clientSecret: 'FOURSQUARE_CLIENT_SECRET',
  versionDate: '20180323',
  currentLatitude: 23.793744,
  currentLongitude: 90.404816

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
