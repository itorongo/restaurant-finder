# Restaurant Finder

Restaurant Finder app build with Angular 7 along with Google map api and Forusquare places api. This app contains random selection of a suitable restaurant within a particular region along with keyword search for restaurants within a specifice range.
## Get the Code
 `git clone https://bitbucket.org/itorongo/restaurant-finder.git`
 
## Quick start

Verify that you are running at least node version 8.x or 10.x. by running node -v in a terminal/console window.  

 1. Install the Angular CLI: `npm install -g @angular/cli`
 2. Open the `restaurant-finder` folder and run `npm install`

This app is using Foursquare places api and Google map api. To use this api properly there needs some configuration.
 
 1. First get the `client_id` & `client_secret` from [Foursquare](https://developer.foursquare.com/places-api)
 2. Get Google map api key from [Google maps platform](https://cloud.google.com/maps-platform/)
 3. Go to `src/app/environmet` in you application and configure the necessary API keys property both in `environment.ts` and `environment.prod.ts` and save the file.
 4. Also you can change the `currentLatitude` and `currentLongitude` property from `environment.ts` and `environment.prod.ts`	

Now you are ready to go..

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
No unit test has been done in this application.

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Why angular

Angular is more structured and matured to build single page application. Component-based architecture provides a higher quality of code and it guides to write clean code and built scalable and reusable component. Also testing is very easy with angular.
 